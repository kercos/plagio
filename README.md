# PlagioBot 
## (Will be renamed to ReWord)

This is a multi-player game to promote creative writing: participants have to come up with a plausible continuation of a beginning of a sentence taken from a book, and have to guess the original continuation among those being suggested.

The game has been extended to fit a classroom setting where the teacher enters the beginning of a sentence and the students propose plausible continuations. In addition, to the “continuation” exercise, the teacher can also use the same game paradigm to undergo “fill-the-gap” and “word-replacement” exercises.

In order to try this game on Telegram, please click on this [link](https://t.me/PlagioBot).

<!-- # License -->
